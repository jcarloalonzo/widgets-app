import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ButtonsScreen extends StatelessWidget {
  static const String name = 'buttons_screen';
  const ButtonsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Buttoms Screens'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.pop();
        },
        child: const Icon(Icons.arrow_back_ios_new_rounded),
      ),
      body: const _ButtonsView(),
    );
  }
}

class _ButtonsView extends StatelessWidget {
  const _ButtonsView();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: Wrap(
          alignment: WrapAlignment.center,
          spacing: 10,
          children: [
            ElevatedButton(
                onPressed: () {}, child: const Text('Elevated Buttom')),
            //
            const ElevatedButton(
                onPressed: null, child: Text('Elevated Disable')),
            //
            ElevatedButton.icon(
                onPressed: () {},
                icon: const Icon(Icons.access_alarm_rounded),
                label: const Text('Elevated Icon')),
            //
            FilledButton(onPressed: () {}, child: const Text('Filled buttom')),
            FilledButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.access_alarm_rounded),
              label: const Text('Filled Icon'),
            ),
//
            OutlinedButton(
              onPressed: () {},
              child: const Text('Outlined buttom'),
            ),
            //
            OutlinedButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.zoom_out_sharp),
              label: const Text('Outlined Icon'),
            ),
            TextButton(onPressed: () {}, child: const Text('Text Buttom')),
            TextButton.icon(
                onPressed: () {},
                icon: const Icon(Icons.zoom_in_map_rounded),
                label: const Text('Text Icon Buttom')),

            const CustomBottom(),
          ],
        ),
      ),
    );
  }
}

class CustomBottom extends StatelessWidget {
  const CustomBottom({super.key});

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    return Material(
      color: colors.primary,
      child: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Hola mundo'),
      ),
    );
  }
}
